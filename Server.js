//Lanciare comando "node Server.js" da riga di comando di Git aprendola dalla cartella "ProvaNode" per vedere il funzionamento
var quadrato = require('./Quadrato.js');
console.log(quadrato.area(4));
console.log(quadrato.perimetro(5));

var hello = require('./Hello.js');
console.log(hello.sayHelloInSpanish("Stefano"));

var express = require('express');
//CREO UN' APPLICAZIONE EXPRESS
var app = express();
app.get('/', function (req, res) {
  res.send('Hello world');
});
app.listen(3000, function () {
  console.log('Listening on port http://localhost:3000');
  //Lanciare da un browser "localhost:3000" per vedere apparire la scritta "Hello world"
});
